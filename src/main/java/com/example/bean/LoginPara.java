package com.example.bean;

/**
 * Created by m2m on 2017/3/27.
 *
 */
public class LoginPara {
    private String clientId;
    private String userName;
    private String password;
    private String captchCode;
    private String captchValue;

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setCaptchCode(String captchCode) {
        this.captchCode = captchCode;
    }

    public void setCaptchValue(String captchValue) {
        this.captchValue = captchValue;
    }

    public String getClientId() {
        return clientId;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getCaptchCode() {
        return captchCode;
    }

    public String getCaptchValue() {
        return captchValue;
    }
}

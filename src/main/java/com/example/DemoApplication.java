package com.example;

import com.example.bean.Audience;
import com.example.filter.HTTPBearerAuthorizeAttribute;
import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@EnableConfigurationProperties(Audience.class)
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	/*@Bean
	public FilterRegistrationBean basicFilterRegistrationBean() {
		FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		HTTPBearerAuthorizeAttribute httpBasicFilter = new HTTPBearerAuthorizeAttribute();
		registrationBean.setFilter(httpBasicFilter);
		List<String> urlPatterns = new ArrayList<String>();
		urlPatterns.add("/user/getuser");
		registrationBean.setUrlPatterns(urlPatterns);
		return registrationBean;
	}*/

	@Bean
	public FilterRegistrationBean jwtFilterRegistrationBean(){
		FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		HTTPBearerAuthorizeAttribute httpBearerFilter = new HTTPBearerAuthorizeAttribute();
		registrationBean.setFilter(httpBearerFilter);
		List<String> urlPatterns = new ArrayList<>();
		urlPatterns.add("/user/getuser");

		registrationBean.setUrlPatterns(urlPatterns);
		return registrationBean;
	}

	@Theme("valo")
	@SpringUI(path = "/vaadin")
	public static class VaadinUI extends UI {

		@Override
		protected void init(VaadinRequest vaadinRequest) {
			Layout layout = new VerticalLayout();
			TextField textField = new TextField();
			Button button = new Button("haha");
			button.addClickListener((Button.ClickListener) clickEvent -> {
				System.out.println(textField.getValue());
			});

			layout.addComponent(button);
			layout.addComponent(textField);
			setContent(layout);
		}
	}
}

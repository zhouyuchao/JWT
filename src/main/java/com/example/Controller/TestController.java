package com.example.Controller;

import com.example.bean.Audience;
import com.example.utils.ResultMsg;
import com.example.utils.ResultStatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by m2m on 2017/3/27.
 */
@RestController
@RequestMapping("/user")
public class TestController {
    @Autowired
    private Audience audience;

    @RequestMapping("getaudience")
    public Object getAudience()
    {
        ResultMsg resultMsg = new ResultMsg(ResultStatusCode.OK.getErrcode(), ResultStatusCode.OK.getErrmsg(), audience);
        return resultMsg;
    }
}
